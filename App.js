import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import HomeScreen from './src/screens/HomeScreen';
import BasicComponentsScreen from './src/screens/BasicComponentsScreen';
import ListScreen from './src/screens/ListScreen';
import ImageScreen from './src/screens/ImageScreen';
import CounterScreen from './src/screens/CounterScreen';
import ColorScreen from './src/screens/ColorScreen';
import ColorPickerScreen from './src/screens/ColorPickerScreen';
import ColorPickerReducerScreen from './src/screens/ColorPickerReducerScreen';
import CounterScreenReducer from './src/screens/CounterReducerScreen';
import TextInputScreen from './src/screens/TextInputScreen';
import PasswordScreen from './src/screens/PasswordScreen';
import LayoutScreen from './src/screens/LayoutScreen'

const navigator = createStackNavigator(
  {
    Home: HomeScreen,
    BasicCompos: BasicComponentsScreen,
    List: ListScreen,
    Image: ImageScreen,
    Counter: CounterScreen,
    Color: ColorScreen,
    ColorPicker: ColorPickerScreen,
    ColorPickerReducer: ColorPickerReducerScreen,
    CounterReducer: CounterScreenReducer,
    TextInput: TextInputScreen,
    Password: PasswordScreen,
    Layout: LayoutScreen
  },
  {
    initialRouteName: 'Home',
    defaultNavigationOptions: {
      title: 'App',
    },
  }
);

export default createAppContainer(navigator);
