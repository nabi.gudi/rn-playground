import React from "react";
import { Text, StyleSheet, View, Image} from "react-native";

const ImageDetail = (props) => {
  return (
    <View>
        <Image source={props.url}></Image>
        <Text>{props.title}</Text>
        <Text>Image score: {props.score}</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  title: {
    fontSize: 45
  },
  text: {
    fontSize: 20

  }
});

export default ImageDetail;
