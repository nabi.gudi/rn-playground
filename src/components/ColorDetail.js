import React from "react";
import { Text, StyleSheet, View, Image} from "react-native";

const ColorDetail = (props) => {
  return (
    <View style={styles.colorContainer}>
      <View style={{
        width: 100,
        height: 100,
        backgroundColor: props.color}}
      />
      <Text style={styles.text}>Color: {props.color}</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  colorContainer: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    marginHorizontal: 20,
    marginTop: 20
  },
  text: {
    fontSize: 20,
    marginLeft: 10

  }
});

export default ColorDetail;
