import React from "react";
import { Text, StyleSheet, View, Button} from "react-native";

const ColorAdjuster = ({color, onDecrease, onIncrease}) => {
  return (
    <View>
      <Text style={styles.subtitle}>{color}</Text>
      <Button 
        onPress={() => onIncrease()}
        title={`More ${color}`}
      />
       <Button 
        onPress={() => onDecrease()}
        title={`Less ${color}`}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  title: {
    fontSize: 45
  },
  subtitle: {
    fontSize: 30
  }
});

export default ColorAdjuster;
