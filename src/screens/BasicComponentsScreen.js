import React from "react";
import { Text, StyleSheet, View } from "react-native";

const BasicComponentsScreen = () => {
  const name = 'Nabila';
  return (
    <View>
      <Text style={styles.title}>First components!</Text>
      <Text style={styles.text}>My name is {name}</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  title: {
    fontSize: 45
  },
  text: {
    fontSize: 20

  }
});

export default BasicComponentsScreen;
