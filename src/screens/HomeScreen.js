import React from "react";
import { Text, StyleSheet, View, Button} from "react-native";

const HomeScreen = ({navigation}) => {
  const name = 'Nabila';
  return (
    <View>
      <Text style={styles.title}>Getting starting with React Native!</Text>
      <Button 
        onPress={() => { navigation.navigate('BasicCompos')}}
        title='Go to components demo'
      />  
      <Button 
        onPress={() => { navigation.navigate('List')}}
        title='Go to list demo'
      />
      <Button 
        onPress={() => { navigation.navigate('Image')}}
        title='Go to image demo'
      />
      <Button 
        onPress={() => { navigation.navigate('Counter')}}
        title='Go to counter demo'
      />
      <Button 
        onPress={() => { navigation.navigate('CounterReducer')}}
        title='Go to counter with reducer demo'
      />
      <Button 
        onPress={() => { navigation.navigate('Color')}}
        title='Go to color demo'
      />
      <Button 
        onPress={() => { navigation.navigate('ColorPicker')}}
        title='Go to color picker demo'
      />
      <Button 
        onPress={() => { navigation.navigate('ColorPickerReducer')}}
        title='Go to color picker with reducer demo'
      />
      <Button 
        onPress={() => { navigation.navigate('TextInput')}}
        title='Go to text input demo'
      />
      <Button 
        onPress={() => { navigation.navigate('Password')}}
        title='Go to password demo'
      />
      <Button 
        onPress={() => { navigation.navigate('Layout')}}
        title='Go to layouts demo'
      />
    </View>
  );
};

const styles = StyleSheet.create({
  title: {
    fontSize: 45
  }
});

export default HomeScreen;
