import React, { useState } from "react";
import { Text, StyleSheet, View, Button} from "react-native";

const CounterScreen = () => {
  const [counter, setCounter] = useState(0);
  return (
    <View>
      <Text style={styles.title}>Counter Screen</Text>
      <Button 
        onPress={() => { setCounter(counter + 1)}}
        title='Increase'
      />
       <Button 
        onPress={() => { setCounter(counter - 1) }}
        title='Decrease'
      />
      <Text style={styles.text}>Current Count: {counter}</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  title: {
    fontSize: 45
  },
  text: {
    fontSize: 20

  }
});

export default CounterScreen;
