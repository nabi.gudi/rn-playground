import React, { useState } from "react";
import { Text, StyleSheet, View, TextInput} from "react-native";

const TextInputScreen = () => {
  const [name, setName] = useState('');
  return (
    <View>
      <Text style={styles.title}>Text input!</Text>
      <Text style={styles.text}>Enter your name: </Text>
      <TextInput 
        style={styles.input}
        autoCapitalize='none'
        autoCorrect={false}
        value={name}
        onChangeText={(newName) => setName(newName)}
        />
      <Text style={styles.text}>My name is {name}</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  title: {
    fontSize: 45
  },
  input: {
    margin: 15,
    padding: 15,
    borderColor: 'black',
    borderWidth: 1,
  },
  text: {
    fontSize: 20

  }
});

export default TextInputScreen;
