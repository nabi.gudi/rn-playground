import React from "react";
import { Text, StyleSheet, View, FlatList} from "react-native";
import ImageDetail from "../components/ImageDetail"

const ImageScreen = () => {
  const imageList = [
    {
      url: require('../../assets/forest.jpg'),
      title: 'forest',
      score: 2
    },{
      url: require('../../assets/beach.jpg'),
      title: 'beach',
      score: 5
    },{
      url: require('../../assets/mountain.jpg'),
      title: 'mountain',
      score: 8
    },
  ];
  return (
    <View>
      <Text style={styles.title}>Image List!</Text>
      <FlatList
        keyExtractor={(image) => image.title}
        data={imageList}
        renderItem={({item}) => {
          return <ImageDetail title={item.title} url={item.url} score={item.score}/>
        }}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  title: {
    fontSize: 35,
    marginBottom: 15
  },
  text: {
    fontSize: 20,
    marginBottom: 10
  }
});

export default ImageScreen;
