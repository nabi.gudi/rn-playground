import React, { useState } from "react";
import { Text, StyleSheet, View, TextInput} from "react-native";

const PasswordScreen = () => {
  const [pass, setPass] = useState('');
  return (
    <View>
      <Text style={styles.title}>Password!</Text>
      <Text style={styles.text}>Enter your password: </Text>
      <TextInput 
        style={styles.input}
        autoCapitalize='none'
        autoCorrect={false}
        value={pass}
        onChangeText={(newPass) => setPass(newPass)}
        secureTextEntry={true}
        />
      <Text style={styles.text}>My password is {pass}</Text>
      {pass.length < 5 ? <Text style={styles.check}>Your password must longer than 5 characters</Text>: null}
    </View>
  );
};

const styles = StyleSheet.create({
  title: {
    fontSize: 45
  },
  input: {
    margin: 15,
    padding: 15,
    borderColor: 'black',
    borderWidth: 1,
  },
  text: {
    fontSize: 20
  },

  check: {
    fontSize: 15,
    color: 'red'
  }
});

export default PasswordScreen;
