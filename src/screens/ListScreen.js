import React from "react";
import { Text, StyleSheet, View, FlatList} from "react-native";

const ListScreen = () => {
  const friends = [
    {name: 'Jimmy', age: 13},
    {name: 'Sam', age: 16},
    {name: 'Lucy', age: 10}
  ];
  return (
    <View>
      <Text style={styles.title}>First List!</Text>
      <FlatList
        keyExtractor={(friend) => friend.name}
        data={friends}
        renderItem={({item}) => {
          return <Text style={styles.text}>{item.name} - Age: {item.age}</Text>
        }}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  title: {
    fontSize: 35,
    marginBottom: 15
  },
  text: {
    fontSize: 20,
    marginBottom: 10
  }
});

export default ListScreen;
