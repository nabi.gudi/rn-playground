import React, { useState } from "react";
import { Text, StyleSheet, View, FlatList, Button} from "react-native";
import ColorDetail from "../components/ColorDetail";

const ColorScreen = () => {
  const [colors, setColors] = useState([]);
  return (
    <View>
      <Text style={styles.title}>List of Random Colors!</Text>
      <Button
        title={'Generate a new color'}
        onPress={() => { setColors([...colors, randomRGB()])}}
      />
      <FlatList
        data={colors}
        renderItem={({item}) => {
          return <ColorDetail color={item}/>
        }}
      />
    </View>
  );
};

const randomRGB = () => {
  const red = Math.floor(Math.random() * 256);
  const green = Math.floor(Math.random() * 256);
  const blue = Math.floor(Math.random() * 256);
  return `rgb(${red}, ${green}, ${blue})`;
}

const styles = StyleSheet.create({
  title: {
    fontSize: 35,
    marginBottom: 15
  },
  text: {
    fontSize: 20,
    marginBottom: 10
  },
  color: {
    width: 30,
    height: 30
  },
});

export default ColorScreen;
