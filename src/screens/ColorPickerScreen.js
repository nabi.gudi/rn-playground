import React, { useState } from "react";
import { Text, StyleSheet, View, Button} from "react-native";
import ColorDetail from "../components/ColorDetail";
import ColorAdjuster from "../components/ColorAdjuster";

const COLOR_JUMP = 15;

const ColorPickerScreen = () => {
  const [red, setRed] = useState(0);
  const [green, setGreen] = useState(0);
  const [blue, setBlue] = useState(0);


  const setColor = (color, change) => {
    switch (color) {
      case 'red': 
        red + change > 255 || red + change < 0 ? null : setRed(red + change);
        return;
      case 'green': 
        green + change > 255 || green + change < 0 ? null : setGreen(green + change);
        return;
      case 'blue': 
        blue + change > 255 || blue + change < 0 ? null : setBlue(blue + change);
        return;
      default:
        return;
    }
  }
  return (
    <View>
      <Text style={styles.title}>Color Picker Screen</Text>
      <ColorAdjuster 
        color='Red'
        onIncrease={() => { setColor('red', COLOR_JUMP)}} 
        onDecrease={() => { setColor('red', -1*COLOR_JUMP)}} 
      />
      <ColorAdjuster 
        color='Green'
        onIncrease={() => { setColor('green', COLOR_JUMP)}} 
        onDecrease={() => { setColor('green', -1*COLOR_JUMP)}} 
      />
      <ColorAdjuster 
        color='Blue'
        onIncrease={() => { setColor('blue', COLOR_JUMP)}} 
        onDecrease={() => { setColor('blue', -1*COLOR_JUMP)}} 
      />
      
      <ColorDetail color={`rgb(${red}, ${green}, ${blue})`} />
    </View>
  );
};

const styles = StyleSheet.create({
  title: {
    fontSize: 45
  },
  subtitle: {
    fontSize: 30

  }
});

export default ColorPickerScreen;
