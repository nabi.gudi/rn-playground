import React from "react";
import { Text, StyleSheet, View} from "react-native";

const LayoutScreen = () => {
  return (
    <View>
      <Text style={styles.title}>Layout Screen!</Text>
      <View style={styles.parent}>
        <Text style={styles.childOne}>Child #1</Text>
        <Text style={styles.childTwo}>Child #2</Text>
        <Text style={styles.childThree}>Child #3</Text>
      </View>
      <View style={styles.exercise}>     
        <View style={styles.main}>
          <View style={styles.one}></View>
          <View style={styles.two}></View>
          <View style={styles.three}></View>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  title: {
    fontSize: 45
  },
  parent: {
    borderWidth: 3,
    borderColor: 'black',
    margin: 20,
    alignItems: 'flex-start',
    flexDirection: 'row',
    justifyContent: 'center'
  },
  childOne: {
    borderWidth: 1,
    borderColor: 'red',
    marginVertical: 20,
    paddingVertical: 10,
  },
  childTwo: {
    borderWidth: 1,
    borderColor: 'red',
    marginVertical: 20,
    paddingVertical: 10,
    flex: 1
  },
  childThree: {
    borderWidth: 1,
    borderColor: 'red',
    marginVertical: 20,
    paddingVertical: 10,
  },
  exercise: {
    borderWidth: 3,
    borderColor: 'black',
    margin: 20,
    height: 110
  },
  titleSection: {
    flex: 1,
    backgroundColor: 'yellow'
  },
  main: {
    flex: 2,
    backgroundColor: 'lightgreen',
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  one: {
    backgroundColor: 'red',
    width: 50,
    height: 50
  },
  two: {
    backgroundColor: 'red',
    width: 50,
    height: 50,
    alignSelf: 'flex-end'
  },
  three: {
    backgroundColor: 'red',
    width: 50,
    height: 50
  }

});

export default LayoutScreen;
